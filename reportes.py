from app import mysql, session
from flask import Flask, render_template, request, redirect, url_for, flash, Blueprint, session
reportes = Blueprint('reportes', __name__)


# Reporte Empleado de los tres mejores empleados del mes
@reportes.route('/productos_mas_vendidos')
def productos_mas_vendidos():
    print("entra reporte")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute("""
            SELECT idProducto,nombre, SUM(cantidad) FROM DetalleVenta de
            INNER JOIN productolaboratorio prolab ON prolab.Producto_idProducto = de.ProductoLaboratorio_idProductoLaboratorio
            INNER JOIN producto pro ON pro.idProducto = prolab.Producto_idProducto
            INNER JOIN venta ve ON ve.idVenta = de.Venta_idVenta
            WHERE ve.fecha BETWEEN '2019-12-01' AND '2019-12-31'
            GROUP BY idProducto,nombre
            ORDER BY SUM(cantidad) DESC
            """)
            data = cur.fetchall()

            return render_template('admin/productos_mas_vendidos.html', session=session, productos=data)

        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")

# Reporte Empleado de los tres mejores empleados del mes
@reportes.route('/empleado_del_mes')
def empleado_del_mes():
    print("entra reporte")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute("""
            SELECT idProducto,nombre, SUM(cantidad) FROM DetalleVenta de
            INNER JOIN productolaboratorio prolab ON prolab.Producto_idProducto = de.ProductoLaboratorio_idProductoLaboratorio
            INNER JOIN producto pro ON pro.idProducto = prolab.Producto_idProducto
            INNER JOIN venta ve ON ve.idVenta = de.Venta_idVenta
            WHERE ve.fecha BETWEEN '2019-01-01' AND '2019-01-31'
            GROUP BY idProducto,nombre
            ORDER BY SUM(cantidad) DESC
            """)
            data = cur.fetchall()

            return render_template('admin/empleado_del_mes.html', session=session, empleados=data)

        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")

# Reporte Empleado de los tres mejores empleados del mes
@reportes.route('/productos_a_vencer')
def productos_vencer():
    print("entra reporte")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute("""
            SELECT idProducto,nombre,prolab.fechaVencimiento FROM Producto pro
            INNER JOIN ProductoLaboratorio prolab ON pro.idProducto = prolab.Producto_idProducto
            WHERE prolab.fechaVencimiento BETWEEN '2019-12-01' AND '2019-12-31'
            ORDER BY prolab.fechaVencimiento  ASC
            """)
            data = cur.fetchall()

            return render_template('admin/productos_a_vencer.html', session=session, productos=data)

        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


# REPORTES ALEJANDRA CAICEDO ----------------------------------------------------------------------------------------

# reporte productos devueltos
@reportes.route('/productos_devueltos')
def productos_devueltos():
    print("entra reporte")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute("""
                         """)
            data = cur.fetchall()
            print('esto es :', data)

            return render_template('admin/productos_devueltos.html', session=session, productos=data)

        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


# reporte productos pedidos
@reportes.route('/productos_pedidos')
def productos_pedidos():
    print("entra reporte")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute("""
                        SELECT p.nombre, p.descripcion, p.idProducto ,COUNT(dp.ProductoLaboratorio_idProductoLaboratorio) AS Pedidos
                        FROM DetallePedido dp
                        INNER JOIN ProductoLaboratorio pl ON dp.ProductoLaboratorio_idProductoLaboratorio = pl.idProductoLaboratorio
                        INNER JOIN Producto p ON pl.idProductoLaboratorio = p.idProducto
                        GROUP BY p.nombre, p.descripcion, p.idProducto
                        ORDER BY Pedidos ASC LIMIT 10""")
            data = cur.fetchall()
            print('esto es :', data)

            return render_template('admin/productos_pedidos.html', session=session, productos=data)

        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


# reporte compras clientes
@reportes.route('/clientes_compras')
def clientes_compras():
    print("entra reporte")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute("""
                        SELECT c.primerNombre, c.primerApellido, c.idCliente, SUM(dv.cantidad) AS Cantidad
                        FROM Cliente c
                        INNER JOIN Venta v ON c.idCliente = v.Cliente_idCliente
                        INNER JOIN DetalleVenta dv ON v.idVenta = dv.Venta_idVenta
                        WHERE v.fecha BETWEEN '2019-07-01' AND '2019-11-31'
                        GROUP BY c.primerNombre, c.primerApellido, c.idCliente
                        ORDER BY Cantidad ASC """)
            data = cur.fetchall()
            print('esto es2 :', data)

            return render_template('admin/clientes_compras.html', session=session, clientes=data)

        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")
