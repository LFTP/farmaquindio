from flask import Flask, render_template, request, redirect, url_for, flash, Blueprint, session
from flask_mysqldb import MySQL


app = Flask(__name__, static_url_path='')


# Configuracion de Conexion a BD
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_PORT'] = 3306
app.config['MYSQL_DB'] = 'farmaquindiodb'
mysql = MySQL(app)

app.secret_key = 'mysecretkey'

from cliente import cliente as cliente_blueprint
from laboratorio import laboratorio as laboratorio_blueprint
from empleado import empleado as empleado_blueprint
from venta import venta as venta_blueprint
from producto import producto as producto_blueprint
from devolucion import devolucion as devolucion_blueprint
from reportes import reportes as reportes_blueprint
from pedido import pedido as pedido_blueprint

app.register_blueprint(cliente_blueprint)
app.register_blueprint(laboratorio_blueprint)
app.register_blueprint(empleado_blueprint)
app.register_blueprint(venta_blueprint)
app.register_blueprint(producto_blueprint)
app.register_blueprint(devolucion_blueprint)
app.register_blueprint(reportes_blueprint)
app.register_blueprint(pedido_blueprint)

@app.route('/',methods=['POST','GET'])
def home():
    if 'error' in  request.form : 
        error = request.form['error']
    else:
        error=None
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            consulta = """
            SELECT em.primerNombre,em.primerApellido,SUM(valor) FROM Empleado em 
            INNER JOIN Venta ve ON em.cedula = ve.Empleado_cedula 
            GROUP BY em.cedula
            """
            cur = mysql.connection.cursor()
            cur.execute(consulta)
            data = cur.fetchall()
            
            empleados = []
            empleadosVentas= []
            maxi =0
            for emp in data:
                empleados.append(str(emp[0]+" "+emp[1]))
                empleadosVentas.append(emp[2])
                num = float(emp[2])
                if num > maxi:
                    maxi = num
            
            ventas = reporteVentasMensuales()
            detallePro = reporteTipoDetalleProducto()
            print(detallePro)
            return render_template('admin/home.html',session = session,error = error,ventas=ventas,
            empleadosReporte=empleados,empleadosVentasReporte=empleadosVentas,maximaVenta=maxi,reporteTipo = detallePro)
        else:
            return render_template('empleado/home.html',session = session,error = error)


def reporteTipoDetalleProducto():
    consulta = """
    SELECT pro.TipoProducto_idTipoProducto,SUM(cantidad) as suma FROM DetalleVenta de
    INNER JOIN ProductoLaboratorio prolab ON de.ProductoLaboratorio_idProductoLaboratorio = prolab.idProductoLaboratorio
    INNER JOIN Producto pro ON pro.idProducto = prolab.idProductoLaboratorio
    INNER JOIN Venta ve ON ve.idVenta = de.Venta_idVenta
    WHERE ve.fecha BETWEEN '2019-01-01' AND '2019-12-31'
    GROUP BY pro.TipoProducto_idTipoProducto
    """
    cur = mysql.connection.cursor()
    cur.execute(consulta)
    data = cur.fetchall()
    r = []
    for d in data:
        r.append(int(d[1]))
    
    return r
    

def reporteVentasMensuales():
    ventas=[]
    for i in range(12):
        consulta = "SELECT SUM(valor) FROM `Venta` WHERE fecha BETWEEN '2019-"+str(i+1)+"-01' AND '2019-"+str(i+1)+"-31'"
        cur = mysql.connection.cursor()
        cur.execute(consulta)
        data = cur.fetchall()

        if data[0][0]!=None:
            ventas.append( data[0][0])
            
        else:
            ventas.append(0)
    return ventas

@app.route('/login', methods=['POST','GET'])
def login():
    error = None
    
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        consulta = 'SELECT * FROM Empleado WHERE email="{0}" AND password="{1}"'.format(request.form['email'],request.form['password'])
        cur.execute(consulta)
        data = cur.fetchall()
        
        if len(data)>0:
            data = data[0]  
            session['email'] = data[5]
            session['nombreCompleto'] = data[1] +" "+ data[2] +" "+  data[3] +" "+data[4]
            session['cedula'] = data[0]    
            session['primerNombre'] = data[1]
            session['segundoNombre'] = data[2]
            session['primerApellido'] = data[3]
            session['segundoApellido'] = data[4]
            

            if request.form['email'] == 'felipe@gmail.com' or request.form['email'] == 'maria@gmail.com' or request.form['email'] == 'fabio@gmail.com':
                session['admin'] = True
            else:
                session['admin'] = False
            
            return redirect(url_for('home'))
        else:
            error = 'Credenciales invalidas.'
    if request.method == 'GET':
        error = request.args.get('error')

    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('cedula', None)
    session.pop('nombreCompleto', None)
    session.pop('primerNombre', None)
    session.pop('segundoNombre', None)
    session.pop('primerApellido', None)
    session.pop('segundoApellido', None)
    session.pop('idEmpleado', None)
    session.pop('admin', None)
      
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(port=3000, debug=True)
