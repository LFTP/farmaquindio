from flask import Flask, render_template, request, redirect, url_for, flash,Blueprint
venta = Blueprint('venta',__name__)
from app import mysql,session



# ventas
@venta.route('/ventas')
def ventas():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM venta ')
    data = cur.fetchall()
    return render_template('ventas.html', ventas=data)


@venta.route('/add_venta', methods=['POST', 'GET'])
def add_venta():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                cur = mysql.connection.cursor()
                consulta = """INSERT INTO `Venta` (  idVenta,fecha,valor) VALUES (NULL,'{0}', '{1}')""".format(request.form['fecha'],request.form['valor'])

                cur.execute(consulta)
                mysql.connection.commit()
            
                return redirect(url_for('venta.tabla_ventas'))
                
            else:
                return render_template('admin/new_venta.html',session = session)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



@venta.route('/perfil/<id>')
def perfilVen(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:

            if id != None:
                print("Entron 3")
                ven={}
                cur = mysql.connection.cursor()
                cur.execute('SELECT * FROM Venta WHERE idVenta="{0}"'.format(id))
                data = cur.fetchall()
                print(data)
                if len(data)>0:
                    data = data[0]
                    ven['idVenta'] = data[0]
                    ven['fecha'] = data[1]
                    ven['valor'] = data[2]
                    return render_template('admin/edit_venta.html',session = session,venta=ven)
        else:
            print("Entron 4")
            if id == None:
                return render_template('perfil.html',session = session,venta=session)
            else:
                return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")
            

@venta.route('/edit_perfil',methods=['POST'])
def edit_perfil():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                consulta = """
                UPDATE Venta
                SET fecha ='{0}', valor='{1}'
                WHERE idVenta='{2}';
                """.format(request.form['fecha'],request.form['valor'],request.form['idVenta'])
                cur = mysql.connection.cursor()
                cur.execute(consulta)
                mysql.connection.commit()
                return redirect(url_for('venta.tabla_ventas'))
            else:
                return render_template('empleado/home.html',session = session, error="No recibió nada para editar")
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



@venta.route('/delete_venta/<string:id>')
def delete_venta(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:    
            cur = mysql.connection.cursor()
            cur.execute('DELETE FROM Venta WHERE idVenta = {0}'.format(id))
            mysql.connection.commit()
            flash("Venta removida", id)
            return redirect(url_for('venta.tabla_ventas'))
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")


@venta.route('/tabla_ventas')
def tabla_ventas():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Venta')
            data = cur.fetchall()

            return render_template('admin/tabla_ventas.html',session = session, ventas=data)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")


# Detalle ventas
@venta.route('/detalleventas')
def detalle_ventas():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM detalleVenta ')
    data = cur.fetchall()
    return render_template('detalleventas.html', detalleventas=data)


@venta.route('/add_detalleventa', methods=['POST'])
def add_detalleventa():

    if request.method == 'POST':
        venta = request.form['idventa']
        fecha = request.form['fecha']
        valor = request.form['valor']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO Venta (idventa,fecha,valor) VALUES (%s,%s,%s)',
                    (venta, fecha, valor))
        mysql.connection.commit()

        flash('Venta agregado exitosamente')
        return redirect(url_for('Index'))


@venta.route('/edit_venta/<id>')
def edit_detalle_venta(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM Venta WHERE idventa = {0}'.format(id))
    data = cur.fetchall()
    return render_template('edit-venta.html', venta=data[0])

#@venta.route('/reporte_empleadoMes/<id>')
#def reporte_empleadoMes(id):