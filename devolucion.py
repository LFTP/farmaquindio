from app import mysql, session
from flask import Flask, render_template, request, redirect, url_for, flash, Blueprint
devolucion = Blueprint('devolucion', __name__)

# acciones devolucion
@devolucion.route('/tabla_devoluciones')
def tabla_devoluciones():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Devolucion ')
            data = cur.fetchall()

            return render_template('admin/tabla_devoluciones.html', session=session, devoluciones=data)
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


@devolucion.route('/add_devolucion', methods=['POST', 'GET'])
def add_devolucion():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                print("entro add devolucion")
                cursor = mysql.connection.cursor()

                return redirect(url_for('devolucion.tabla_devoluciones'))

            else:
                return render_template('admin/new_devolucion.html', session=session)
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")

@devolucion.route('/devolucion/<id>')
def devolucionId(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if id != None:
                cli={}
                cur = mysql.connection.cursor()
                cur.execute('SELECT * FROM Devolucion WHERE idDevolucion="{0}"'.format(id))
                data = cur.fetchall()
                print(data)
                if len(data)>0:
                    data = data[0]
                    cli['idDevolucion'] = data[0]
                    cli['fecha'] = data[1]
                    cli['Factura_idFactura'] = data[2]
                    cli['Empleado_cedula'] = data[3]
                    
                    return render_template('admin/edit_devolucion.html',session = session,devolucion=cli)
            else:
                return redirect(url_for('devolucion.tabla_devoluciones'))
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")

@devolucion.route('/delete_devolucion/<string:id>')
def delete_devolucion(id):
    print("entro delete dev")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('DELETE FROM Devolucion WHERE idDevolucion = {0}'.format(id))
            mysql.connection.commit()
            flash("Devolucion removido correctamente", id)
            return redirect(url_for('devolucion.tabla_devoluciones'))
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")

# Factura compra
