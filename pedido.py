from flask import Flask, render_template, request, redirect, url_for, flash, Blueprint
pedido = Blueprint('pedido', __name__)
from app import mysql,session


#pedidos
@pedido.route('/tabla_pedidos')
def tabla_pedidos():
    
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute("""
            SELECT idPedido, em.primerNombre,em.segundoNombre,pe.fechaCompra,pe.valor FROM Pedido pe
            INNER JOIN Empleado em ON pe.Empleado_cedula = em.cedula ORDER BY pe.fechaCompra
            """)
            data = cur.fetchall()

            return render_template('admin/tabla_pedidos.html',session = session,pedidos=data)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")




@pedido.route('/pedido/<id>')
def pedidoid(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            
            if id != None:
                pedido={}

                cur = mysql.connection.cursor()
                consulta = """
                SELECT em.primerNombre,em.primerApellido, pe.fechaCompra,pe.valor FROM Pedido pe
                INNER JOIN Empleado em ON pe.Empleado_cedula = em.cedula
                WHERE idPedido={0} """.format(id)
                cur.execute(consulta)
                data = cur.fetchall()
                
                if len(data)>0:


                    consulta = """
                    SELECT pro.nombre,lab.nombre,prolab.costo,prolab.fechaVencimiento,prolab.presentacion,prolab.idProductoLaboratorio
                    FROM ProductoLaboratorio prolab
                    INNER JOIN Producto pro ON prolab.Producto_idProducto = pro.idProducto
                    INNER JOIN Laboratorio lab ON prolab.Laboratorio_idLaboratorio = lab.idLaboratorio
                    """
                    cur.execute(consulta)
                    proLab = cur.fetchall()
                    
                    data = data[0]
                    pedido['nombreEmpleado'] = data[0] +" "+ data[1]
                    pedido['fecha'] = data[2]
                    pedido['valor'] = data[3]
                    consulta = """
                    SELECT pro.nombre,prolab.presentacion,prolab.fechaVencimiento,prolab.costo,de.cantidad,prolab.idProductoLaboratorio
                    FROM DetallePedido de
                    INNER JOIN ProductoLaboratorio prolab ON prolab.idProductoLaboratorio = de.ProductoLaboratorio_idProductoLaboratorio
                    INNER JOIN Producto pro ON prolab.Producto_idProducto = pro.idProducto
                    INNER JOIN Laboratorio lab ON prolab.Laboratorio_idLaboratorio = lab.idLaboratorio
                    WHERE Pedido_idPedido="{0}"
                    """.format(id)
                    cur.execute(consulta)
                    data = cur.fetchall()
                    detalles = []
                    cont = 0 
                    for detPed in data:
                        cont+=1
                        d = []
                        d.append(detPed[0]+" "+detPed[1])
                        d.append(detPed[2])
                        d.append(detPed[3])
                        d.append(detPed[4])
                        d.append(detPed[5])
                        d.append(cont)
                        
                        detalles.append(d)
                        #detalles['nombre'+str(cont)] = detPed[0]+" "+detPed[3]
                        #detalles['costo'+str(cont) ] =  detPed[1]
                        #detalles['cantidad'+str(cont) ] =  detPed[4]
                        #detalles['fechaVencimiento'+str(cont) ] =  detPed[2]

                    
             
                    
                    return render_template('admin/perfilPedido.html',session = session,pedido=pedido,productosLab=proLab,detallePedido=detalles,conta =cont,idPed=id)
            else:
                return redirect(url_for('pedido.tabla_pedidos'))
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")


@pedido.route('/edit_perfil',defaults={'id': None})            
@pedido.route('/edit_perfil/<id>',methods=['POST'])
def edit_pedido(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                cur = mysql.connection.cursor()

                cedula = request.form['cedula']
                fecha = request.form['fecha']
                valorPedido = request.form['valorTotalPedido']

                consulta = """DELETE pe,de 
                FROM pedido pe 
                LEFT JOIN DetallePedido de 
                    ON pe.idPedido = de.Pedido_idPedido
                WHERE pe.idPedido ={0} """.format(id)
                cur.execute(consulta)
                mysql.connection.commit()
                
                consulta = """
                INSERT INTO `Pedido` (`idPedido`, `fechaCompra`, `valor`, `Empleado_cedula`) VALUES
                (NULL,'{0}', '{1}', '{2}'); """.format(fecha,valorPedido,cedula)
                cur.execute(consulta)
                mysql.connection.commit()
                
                consulta="SELECT idPedido FROM Pedido ORDER BY idPedido DESC LIMIT 1"
                cur.execute(consulta)
                idPe = cur.fetchall()[0][0]
                
                
                

                
                filas = int(request.form['contadorFilas'])
                for i in range(1,filas+1):
                    if 'contadorProducto'+str(i) in request.form:
                        idProd = request.form['contadorProducto'+str(i)]
                        cantidad = request.form['cant'+str(i)]
                        
                        consulta = """
                        INSERT INTO `DetallePedido` (`cantidad`, `Pedido_idPedido`, `ProductoLaboratorio_idProductoLaboratorio`) VALUES
                        ({0}, '{1}', '{2}')""".format(cantidad,idPe,idProd)
                        cur.execute(consulta)
                        mysql.connection.commit()

                return redirect(url_for('pedido.tabla_pedidos'))
            else:
                return render_template('empleado/home.html',session = session, error="No recibió nada para editar")
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")


@pedido.route('/add_pedido', methods=['POST', 'GET'])
def add_pedido():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                cedula = request.form['cedula']
                fecha = request.form['fecha']
                valorPedido = request.form['valorTotalPedido']

                cur = mysql.connection.cursor()
                
                consulta = """
                INSERT INTO `Pedido` (`idPedido`, `fechaCompra`, `valor`, `Empleado_cedula`) VALUES
                (NULL,'{0}', '{1}', '{2}'); """.format(fecha,valorPedido,cedula)
                cur.execute(consulta)
                mysql.connection.commit()

                
                consulta="SELECT idPedido FROM Pedido ORDER BY idPedido DESC LIMIT 1"
                cur.execute(consulta)
                idPe = cur.fetchall()[0][0]
                

                
                filas = int(request.form['contadorFilas'])
                for i in range(1,filas+1):                    
                    if 'contadorProducto'+str(i) in request.form:
                        idProd = request.form['contadorProducto'+str(i)]
                        cantidad = request.form['cant'+str(i)]
                        
                        consulta = """
                        INSERT INTO `DetallePedido` (`cantidad`, `Pedido_idPedido`, `ProductoLaboratorio_idProductoLaboratorio`) VALUES
                        ({0}, '{1}', '{2}')""".format(cantidad,idPe,idProd)
                        
                        cur.execute(consulta)
                        mysql.connection.commit()

                return redirect(url_for('pedido.tabla_pedidos'))
            else:
                cur = mysql.connection.cursor()
                consulta = """
                SELECT pro.nombre,lab.nombre,prolab.costo,prolab.fechaVencimiento,prolab.presentacion,prolab.idProductoLaboratorio
                FROM ProductoLaboratorio prolab
                INNER JOIN Producto pro ON prolab.Producto_idProducto = pro.idProducto
                INNER JOIN Laboratorio lab ON prolab.Laboratorio_idLaboratorio = lab.idLaboratorio
                """
                cur.execute(consulta)
                data = cur.fetchall()
                
                return render_template('admin/new_pedido.html',session = session,productosLab=data)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



@pedido.route('/delete_pedido/<string:id>')
def delete_pedido(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:    
            cur = mysql.connection.cursor()
            consulta = """DELETE pe,de 
            FROM pedido pe 
            LEFT JOIN DetallePedido de 
                ON pe.idPedido = de.Pedido_idPedido
            WHERE pe.idPedido ={0} """.format(id)
            cur.execute(consulta)
            mysql.connection.commit()
            flash("Contacto removido", id)
            return redirect(url_for('pedido.tabla_pedidos'))
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



