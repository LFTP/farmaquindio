from flask import Flask, render_template, request, redirect, url_for, flash,Blueprint
producto = Blueprint('producto',__name__)
from app import mysql,session

@producto.route('/perfilPro/<id>')
def perfilPro(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:

            if id != None:
                print("Entron 3")
                pro={}
                cur = mysql.connection.cursor()
                cur.execute('SELECT * FROM Producto WHERE idProducto="{0}"'.format(id))
                data = cur.fetchall()
                print(data)
                if len(data)>0:
                    data = data[0]
                    pro['idProducto'] = data[0]
                    pro['nombre'] = data[1]
                    pro['descripcion'] = data[2]
                    return render_template('admin/edit_producto.html',session = session,producto=pro)
                else:
                    return render_template('admin/tabla_productos.html',session = session,producto=pro)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")
            
@producto.route('/edit_producto',methods=['POST'])
def edit_producto():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                consulta = """
                UPDATE Producto
                SET nombre ='{0}', descripcion='{1}'
                WHERE idproducto='{2}';
                """.format(request.form['nombre'],request.form['descripcion'],request.form['idProducto'])
                cur = mysql.connection.cursor()
                cur.execute(consulta)
                mysql.connection.commit()
                return redirect(url_for('producto.tabla_productos'))
            else:
                return render_template('empleado/home.html',session = session, error="No recibió nada para editar")
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")

@producto.route('/tabla_productos')
def tabla_productos():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Producto ')
            data = cur.fetchall()

            return render_template('admin/tabla_productos.html',session = session,productos=data)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")


@producto.route('/add_producto', methods=['POST','GET'])
def add_producto():
     if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
     else:
        if session['admin'] == True:
            if request.method == 'POST':
                cur = mysql.connection.cursor()
                consulta = """INSERT INTO `Producto` (  idProducto,nombre, descripcion) VALUES (NULL,'{0}', '{1}')""".format(request.form['nombre'],request.form['descripcion'])

                cur.execute(consulta)
                mysql.connection.commit()
            
                return redirect(url_for('producto.tabla_productos'))
                
            else:
                return render_template('admin/new_producto.html',session = session)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



# Funciona el delete mientras no tenga un productoLaboratorio asociado. En caso de que lo tenga toca borrar primero
# productoLaboratorio para que funcione, si eso pasaba, el de ventas tambien esta presentando problemas
@producto.route('/delete_producto/<string:id>')
def delete_producto(id):
    print("entro delete pro")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            cursor = mysql.connection.cursor()
            cursor.execute(
                'DELETE FROM Producto WHERE idProducto = {0}' .format(id))
            mysql.connection.commit()
            
            return redirect(url_for('producto.tabla_productos'))


# Detalle productos
@producto.route('/detalleproductos')
def detalle_productos():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM tipo_producto ')
    data = cur.fetchall()
    return render_template('tipo_producto.html', tipo_producto=data)


@producto.route('/add_tipo_producto', methods=['POST'])
def add_tipo_producto():

    if request.method == 'POST':
        producto = request.form['idproducto']
        nombre = request.form['nombre']
        descripcion = request.form['descripcion']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO Producto (idproducto,nombre,descripcion) VALUES (%s,%s,%s)',
                    (producto, nombre, descripcion))
        mysql.connection.commit()

        flash('Producto agregado exitosamente')
        return redirect(url_for('Index'))


@producto.route('/edit_producto/<id>')
def edit_detalle_producto(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM Producto WHERE idproducto = {0}'.format(id))
    data = cur.fetchall()
    return render_template('edit-producto.html', producto=data[0])
