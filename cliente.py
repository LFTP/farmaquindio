from flask import Flask, render_template, request, redirect, url_for, flash,Blueprint,session
cliente = Blueprint('cliente',__name__)
from app import mysql,session


@cliente.route('/perfil/<id>')
def perfil(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:

            if id != None:
                print("Entron 3")
                cli={}
                cur = mysql.connection.cursor()
                cur.execute('SELECT * FROM Cliente WHERE idCliente="{0}"'.format(id))
                data = cur.fetchall()
                print(data)
                if len(data)>0:
                    data = data[0]
                    cli['idCliente'] = data[0]
                    cli['primerNombre'] = data[1]
                    cli['segundoNombre'] = data[2]
                    cli['primerApellido'] = data[3]
                    cli['segundoApellido'] = data[4]
                    return render_template('admin/edit_cliente.html',session = session,cliente=cli)
        else:
            print("Entron 4")
            if id == None:
                return render_template('perfil.html',session = session,cliente=session)
            else:
                return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")
            
@cliente.route('/edit_perfil',methods=['POST'])
def edit_perfil():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                consulta = """
                UPDATE Cliente
                SET primerNombre ='{0}', segundoNombre='{1}', primerApellido='{2}',segundoApellido='{3}'
                WHERE idCliente='{4}';
                """.format(request.form['primerNombre'],request.form['segundoNombre'],request.form['primerApellido'],request.form['segundoApellido'],request.form['idCliente'])
                cur = mysql.connection.cursor()
                cur.execute(consulta)
                mysql.connection.commit()
                return redirect(url_for('cliente.tabla_clientes'))
            else:
                return render_template('empleado/home.html',session = session, error="No recibió nada para editar")
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



#clientes
@cliente.route('/tabla_clientes')
def tabla_clientes():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Cliente ')
            data = cur.fetchall()

            return render_template('admin/tabla_clientes.html',session = session,clientes=data)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")




@cliente.route('/add_cliente', methods=['POST', 'GET'])
def add_cliente():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                cur = mysql.connection.cursor()
                consulta = """INSERT INTO `Cliente` (  idCliente,primerNombre, segundoNombre, primerApellido, segundoApellido) VALUES (NULL,'{0}', '{1}', '{2}', '{3}')""".format(request.form['primerNombre'],request.form['segundoNombre'], request.form['primerApellido'], request.form['segundoApellido'])

                cur.execute(consulta)
                mysql.connection.commit()
            
                return redirect(url_for('cliente.tabla_clientes'))
                
            else:
                return render_template('admin/new_cliente.html',session = session)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



@cliente.route('/delete_cliente/<string:id>')
def delete_cliente(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:    
            cur = mysql.connection.cursor()
            cur.execute('DELETE FROM Cliente WHERE idCliente = {0}'.format(id))
            mysql.connection.commit()
            flash("Contacto removido", id)
            return redirect(url_for('cliente.tabla_clientes'))
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



