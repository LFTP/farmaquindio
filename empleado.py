from flask import Flask, render_template, request, redirect, url_for, flash,Blueprint
empleado = Blueprint('empleado',__name__)
from app import mysql,session





@empleado.route('/perfil', defaults={'id': None})
@empleado.route('/perfil/<id>')
def perfil(id):    
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        print("Entron 1")
        if session['admin'] == True:
            print("Entron 2 sa", id)
            if id != None:
                print("Entron 3")
                emp = {}
                cur = mysql.connection.cursor()
                cur.execute('SELECT * FROM Empleado WHERE cedula="{0}"'.format(id))
                
                data = cur.fetchall()
                print(data)
                if len(data)>0:
                    data = data[0]
                    emp['cedula'] = data[0]
                    emp['primerNombre'] = data[1]
                    emp['segundoNombre'] = data[2]
                    emp['primerApellido'] = data[3]
                    emp['segundoApellido'] = data[4]
                    emp['email'] = data[5]
                    return render_template('perfil.html',session = session,empleado=emp)
        else:
            print("Entron 4")
            if id == None:
                return render_template('perfil.html',session = session,empleado=session)
            else:
                return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")
        return render_template('perfil.html',session = session,empleado=session)
            

@empleado.route('/edit_perfil',methods=['POST'])

def edit_perfil():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if request.method == 'POST':
            consulta = """
            UPDATE empleado
            SET email='{0}', primerNombre ='{1}', segundoNombre='{2}', primerApellido='{3}',segundoApellido='{4}'
            WHERE cedula='{5}';
            """.format(request.form['email'],request.form['primerNombre'],request.form['segundoNombre'],request.form['primerApellido'],request.form['segundoApellido'],request.form['cedula'])
            cur = mysql.connection.cursor()
            cur.execute(consulta)
            mysql.connection.commit()
            if request.form['cedula'] == session['cedula']:

                session['email'] = request.form['email']
                session['nombreCompleto'] = request.form['primerNombre']+" "+request.form['segundoNombre']+" "+request.form['primerApellido']+" "+request.form['segundoApellido']
                session['primerNombre'] = request.form['primerNombre']
                session['segundoNombre'] = request.form['segundoNombre']
                session['primerApellido'] = request.form['primerApellido']
                session['segundoApellido'] = request.form['segundoApellido']

        if session['admin'] == True:
            return redirect(url_for('empleado.tabla_empleados'))
        else:
            return redirect(url_for('empleado.perfil'))


# empleados
@empleado.route('/tabla_empleados')
def tabla_empleados():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:

            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Empleado ')
            data = cur.fetchall()
            return render_template('admin/tabla_empleados.html',session = session,empleados=data)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")

    


@empleado.route('/add_empleado', methods=['POST','GET'])
def add_empleado():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                cur = mysql.connection.cursor()
                cur.execute('SELECT * FROM Empleado WHERE cedula="{0}" or email="{1}"; '.format(request.form['cedula'],request.form['email']))
                data = cur.fetchall()
                print('LEGO ESTOS DATOS ',data)
                if len(data) == 0:

                    consulta = """
                    INSERT INTO `Empleado` (`cedula`, `primerNombre`, `segundoNombre`, `primerApellido`, `segundoApellido`, `email`, `password`) VALUES
                    ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')""".format(request.form['cedula'],request.form['primerNombre'],request.form['segundoNombre'],request.form['primerApellido'],request.form['segundoApellido'],request.form['email'],request.form['password'])
                    cur.execute(consulta)
                    mysql.connection.commit()

                    flash('empleado agregado exitosamente')
                    return redirect(url_for('empleado.tabla_empleados'))
                else:
                    error ='Cedula o correo ya se encuentra registrado'
                    return render_template('admin/new_empleado.html',session = session, error=error)
            else:
                return render_template('admin/new_empleado.html',session = session)
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")



@empleado.route('/delete_empleado/<string:id>')
def delete_empleado(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login',error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('DELETE FROM Empleado WHERE cedula = {0}'.format(id))
            mysql.connection.commit()
            flash("Contacto removido", id)
            return redirect(url_for('empleado.tabla_empleados'))
        else:
            return render_template('empleado/home.html',session = session, error="No tiene los permisos para acceder a esta página")

