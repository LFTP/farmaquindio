from app import mysql, session
from flask import Flask, render_template, request, redirect, url_for, flash, Blueprint
laboratorio = Blueprint('laboratorio', __name__)


# acciones laboratorio
@laboratorio.route('/tabla_laboratorios')
def tabla_laboratorios():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Laboratorio ')
            data1 = cur.fetchall()
            print('esto es:', data1)

            return render_template('admin/tabla_laboratorios.html', session=session, laboratorios=data1)
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


def prueba():
    cur = mysql.connection.cursor()
    cur.execute("""
                        SELECT p.nombre, p.descripcion, p.idProducto ,COUNT(dp.ProductoLaboratorio_idProductoLaboratorio) AS Pedidos 
                        FROM DetallePedido dp
                        INNER JOIN ProductoLaboratorio pl ON dp.ProductoLaboratorio_idProductoLaboratorio = pl.idProductoLaboratorio 
                        INNER JOIN Producto p ON pl.idProductoLaboratorio = p.idProducto
                        GROUP BY p.nombre, p.descripcion, p.idProducto  """)
    data = cur.fetchall()
    print('esto es 2 :', data)
    return data[0]


@laboratorio.route('/add_laboratorio', methods=['POST', 'GET'])
def add_laboratorio():
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                print("entro add laboratorio")
                cursor = mysql.connection.cursor()

                consulta = """INSERT INTO `Laboratorio` ( idLaboratorio, nombre, direccion) VALUES (NULL,'{0}', '{1}' )""".format(
                    request.form['nombre'], request.form['direccion'])

                cursor.execute(consulta)
                mysql.connection.commit()

                return redirect(url_for('laboratorio.tabla_laboratorios'))

            else:
                return render_template('admin/new_laboratorio.html', session=session)
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


@laboratorio.route('/laboratorio/<id>')
def laboratorioId(id):
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            if id != None:
                cli = {}
                cur = mysql.connection.cursor()
                cur.execute(
                    'SELECT * FROM Laboratorio WHERE idLaboratorio="{0}"'.format(id))
                data = cur.fetchall()
                print(data)
                if len(data) > 0:
                    data = data[0]
                    cli['idLaboratorio'] = data[0]
                    cli['nombre'] = data[1]
                    cli['direccion'] = data[2]

                    return render_template('admin/perfilLab.html', session=session, laboratorio=cli)
            else:
                return redirect(url_for('laboratorio.tabla_laboratorios'))
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


@laboratorio.route('/delete_laboratorio/<string:id>')
def delete_laboratorio(id):
    print("entro delete lab")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            cur = mysql.connection.cursor()
            cur.execute(
                'DELETE FROM Laboratorio WHERE idLaboratorio = {0}'.format(id))
            mysql.connection.commit()
            flash("Laboratorio removido correctamente", id)
            return redirect(url_for('laboratorio.tabla_laboratorios'))
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")


@laboratorio.route('/edit_laboratorio', methods=['POST', 'GET'])
def edit_laboratorio():
    print("entro edit lab")
    if not 'admin' in session:
        err = "Por favor ingrese para poder continuar."
        return redirect(url_for('login', error=err))
    else:
        if session['admin'] == True:
            if request.method == 'POST':
                consulta = """
                UPDATE Laboratorio
                SET nombre ='{0}', direccion='{1}'
                WHERE idLaboratorio='{2}';
                """.format(request.form['nombre'], request.form['direccion'], request.form['idLaboratorio'])
                cur = mysql.connection.cursor()
                cur.execute(consulta)
                mysql.connection.commit()
                flash("Laboratorio actualizado correctamente")
                return redirect(url_for('laboratorio.tabla_laboratorios'))
            else:
                return render_template('admin/home.html', session=session, error="No recibió nada para editar")
        else:
            return render_template('empleado/home.html', session=session, error="No tiene los permisos para acceder a esta página")
